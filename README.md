# OBSOLETE AND VERY OLD

Backtested, ML-driven strategies for basic investment income.

Run the various functions in `analysis` to generate reports.

Explore `stock_data` for live information about any security.

Play with `backtest` for developing and optimizing enter/exit models.

Implement `transact` to put your autonomous trader into production.
