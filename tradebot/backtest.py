"""Simulate the performance of various strategies against historical data.

Wraps backtrader with my own strategies/indicators:
    https://www.backtrader.com/docu/quickstart/quickstart
"""
# pylint: disable=missing-class-docstring,missing-function-docstring
import datetime
import backtrader


class BaseStrategy(backtrader.Strategy):  # pylint: disable=too-many-ancestors
    """General:

    * Lines are initialized during __init__(). Index [0] is the current value.
        Index [-1] is the previous value, etc.
    * self.data points to self.datas[0], the current bar. This applies to other
        lines the same, e.g. self.sma points to self.sma[0].
    """

    def __init__(self):
        super().__init__()
        # Indicators for the plot.
        # pylint: disable=unexpected-keyword-arg,no-member,too-many-function-args
        backtrader.indicators.ExponentialMovingAverage(period=25)
        backtrader.indicators.WeightedMovingAverage(period=25, subplot=True)
        backtrader.indicators.StochasticSlow()
        backtrader.indicators.MACDHisto()
        rsi = backtrader.indicators.RSI()
        backtrader.indicators.SmoothedMovingAverage(rsi, period=10)
        backtrader.indicators.ATR(plot=False)
        # pylint: enable=unexpected-keyword-arg,no-member,too-many-function-args

    def log(self, msg):
        date = self.data.datetime.date(0).isoformat()
        print(f'[{date}] {msg}')

    def notify_order(self, order):
        if order.status == order.Completed:
            prefix = '-- buy' if order.isbuy() else '---- sell' if order.issell() else 'unknown'
            self.log('{} order executed @{:.2f}'.format(prefix, order.executed.price))

    def notify_trade(self, trade):
        if trade.isclosed:
            self.log('trade profit: {:.2f} ({:.2f} with commission)'.format(trade.pnl, trade.pnlcomm))  # pylint: disable=line-too-long


class RelativeToAverage(BaseStrategy):  # pylint: disable=too-many-ancestors
    """Buy if close is greater than the simple moving average (over the desired period),
    sell if below. This is the same as "buy when convex, sell when concave".
    """

    params = (
        ('period', 15),
    )

    def __init__(self):
        super().__init__()
        self.sma = backtrader.indicators.SimpleMovingAverage(period=self.params.period)  # pylint: disable=no-member

    def next(self):
        if not self.position:
            if self.data.close > self.sma:
                self.buy()
        else:
            if self.data.close < self.sma:
                self.sell()


def main(basis, start_date, end_date, ticker, quantity, strategy, params):  # pylint: disable=too-many-arguments
    # Initialize.
    cerebro = backtrader.Cerebro()
    cerebro.broker.setcommission(0.00)
    cerebro.broker.setcash(basis)
    cerebro.addsizer(backtrader.sizers.FixedSize, stake=quantity)
    # Define strategy.
    cerebro.addstrategy(strategy, **params)
    # Supply data.
    data = backtrader.feeds.YahooFinanceData(
        dataname=ticker,
        fromdate=start_date,
        todate=end_date,
        reverse=False,
        period='d',  # each bar is one day
    )
    cerebro.adddata(data)
    # Run the simulation.
    start = cerebro.broker.getvalue()
    cerebro.run()
    finish = cerebro.broker.getvalue()
    change = finish - start
    performance = 100 * change / start
    print('Starting value: {:.2f}'.format(start))
    print('Finishing value: {:.2f}'.format(finish))
    print('Profit: ${:.2f}'.format(change))
    print('Portfolio performance: {:.2f}%'.format(performance))
    cerebro.plot()

if __name__ == '__main__':
    main(
        basis=10000,
        start_date=datetime.datetime(2019, 1, 1),
        end_date=datetime.datetime(2019, 11, 1),
        ticker='msft',
        quantity=10,
        strategy=RelativeToAverage,
        params={'period': 15},
    )
