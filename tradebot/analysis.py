"""Suggests purchases, based on securites that meet certain criteria.
"""
# pylint: disable=missing-function-docstring
from multiprocessing import Pool
from datetime import datetime
from operator import itemgetter
from dateutil.relativedelta import relativedelta

from tradebot.stock_data import (
    get_tickers,
    get_slope,
    get_delta,
    get_dividend_yield,
)


def calc_forward_returns(ticker):
    print(ticker)
    try:
        one_year_ago = datetime.now() - relativedelta(years=1)
        start_date = one_year_ago.strftime('%Y-%m-%d')
        slope = get_slope(ticker=ticker, start_date=start_date, basis=-1)
    except Exception as ex:  # pylint: disable=broad-except
        print(ex)
        slope = 0
    try:
        yld = get_dividend_yield(ticker=ticker, mode='Forward')
    except Exception as ex:  # pylint: disable=broad-except
        print(ex)
        yld = 0
    return slope, yld


def calc_trailing_returns(ticker):
    print(ticker)
    try:
        slope = get_delta(ticker=ticker)
    except Exception as ex:  # pylint: disable=broad-except
        print(ex)
        slope = 0
    try:
        yld = get_dividend_yield(ticker=ticker, mode='Trailing')
    except Exception as ex:  # pylint: disable=broad-except
        print(ex)
        yld = 0
    return slope, yld


def total_return(func):
    ticker_data = get_tickers()
    tickers = [ticker['ticker'] for ticker in ticker_data]
    with Pool() as pool:
        results = pool.map(func, tickers)
        for data, result in zip(ticker_data, results):
            slope, yld = result
            data['slope'] = slope
            data['yield'] = yld
            data['total'] = slope + yld
    sorted_results = sorted(ticker_data, key=itemgetter('total'), reverse=True)
    fname = 'roi_{}.txt'.format('forward' if 'forward' in func.__name__ else 'trailing')
    with open(fname, 'w') as output_file:
        output_file.write(
            ' rank '
            ' symbol '
            ' total-return (%/yr) '
            ' ttm-slope (%/yr) '
            ' div-yield (%/yr) '
            ' price ($/share) '
            ' market cap ($) '
            '\n'
        )
        for index, security in enumerate(sorted_results):
            output_file.write(
                f'{index+1:>5} '
                f'{security["ticker"]:>7} '
                f'{security["total"]:20.2f} '
                f'{security["slope"]:17.2f} '
                f'{security["yield"]:17.2f} '
                f'{security["price"]:16.2f} '
                f'{round(security["cap"]/1000000):>14}m '
                f'\n'
            )


def total_return_forward():
    """Combines the slope of the past year's performance (via simple linear regression)
    as $/yr, then normalizes it on the CURRENT stock price, as a prediction for growth
    in the coming year. Also calculates the forward div yield; most recent div,
    annualized (say, 15 div payouts per year), divided by current stock price. Then it adds
    both for total return, in %/year, and sorts by the best.

    Ultimately, this predicts the best ROI stocks given the current state.
    """
    total_return(calc_forward_returns)


def total_return_trailing():
    """Finds the delta between the last and first price point in the previous year,
    calculating total growth if you would have investing exactly 12 months ago, normalized
    by the price at that time into %/year. Also calculates the trailing div yield, from the
    actual payouts of the last twelve months. Then it adds both for total return, in %/year,
    and sorts by the best.

    Ultimately, this gives the best ROI performers of last year.
    """
    total_return(calc_trailing_returns)
