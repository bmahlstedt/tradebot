"""Self-contained conveniences.
"""


def unspell_money(num_str):
    """Takes a string with the letters m/b/t for million/billion/trillion
    and converts it into a proper int.

    Args:
        num_str (str): must contain one of mMbBtT, at the end

    Returns:
        (int)
    """
    mult_map = {
        'm': 1e6, 'M': 1e6,
        'b': 1e9, 'B': 1e9,
        't': 1e12, 'T': 1e12,
    }
    return int(float(num_str[:-1])*mult_map[num_str[-1]])
