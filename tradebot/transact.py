"""The buy/sell capability of the bot.

Wraps robin-stocks:
    http://www.robin-stocks.com/en/latest/index.html
"""
# pylint: disable=invalid-name
import robin_stocks as rh

from tradebot.creds import username, password  # pylint: disable=import-error,no-name-in-module

# This requires you to check your phone and input the 2FA code.
rh.login(username, password)

# View information about your profile.
profile_summary = rh.account.build_user_profile()
profile = {
    'account': rh.profiles.load_account_profile(),
    'basic': rh.profiles.load_basic_profile(),
    'investment': rh.profiles.load_investment_profile(),
    'portfolio': rh.profiles.load_portfolio_profile(),
    'security': rh.profiles.load_security_profile(),
    'user': rh.profiles.load_user_profile(),
}

# View information about your holdings.
holdings_summary = rh.account.build_holdings()
all_holdings = rh.account.get_all_positions()
holdings = rh.account.get_current_positions()

# You can get a ton of stock info from rh.stocks.X.
# Reference: http://www.robin-stocks.com/en/latest/functions.html#getting-stock-information

# Buy examples.
rh.order_buy_market(symbol='TSLA', quantity=10)
rh.order_buy_limit(symbol='TSLA', quantity=10, limitPrice=200.00)

# Sell examples.
rh.order_sell_market(symbol='TSLA', quantity=10)
rh.order_sell_limit(symbol='TSLA', quantity=10, limitPrice=300.00)

# You can also do stop orders, crypto, options, change from the default of GTC, more.
