"""This module fetches raw information about securities, and does some light
analysis to reformat the data when necessary. All network requests should be
contained here.

Yahoo's API doesn't have complete data. Example: full history is not available
some symbols (preventing any fitting), but the 52-week change is.
"""
# pylint:disable=invalid-name,missing-docstring,too-many-locals
import codecs
from contextlib import closing
import csv
import math
import numbers
import requests
from requests_html import HTMLSession
from pandas.io.json import json_normalize, loads
import pandas as pd
from sklearn.linear_model import LinearRegression
from matplotlib import pyplot

from tradebot.util import unspell_money


def get_tickers():
    """Reaches out to NASDAQ and fetches all symbols on the NYSE. Ignores any
    that don't have a listed price or market cap.

    Returns:
        [{'ticker': str, 'cap': int, 'price': float}, ...]
    """
    nyse_url = (
        'https://old.nasdaq.com/screening/companies-by-name.aspx'
        '?letter=0&exchange=nyse&render=download'
    )
    # The stream and iterdecode are just generators to keep the whole csv out of memory.
    with closing(requests.get(nyse_url, stream=True)) as resp:
        # Decode because the raw response is bytes.
        data = csv.DictReader(codecs.iterdecode(resp.iter_lines(), 'utf-8'))
        # Iterate over the data and filter out the securites that match my criteria.
        results = [
            {'ticker': row['Symbol'].strip(),
             'cap': unspell_money(row['MarketCap'].replace('$', '')),
             'price': float(row['LastSale'])}
            for row in data
            if 'n/a' not in [row['MarketCap'], row['LastSale']]
        ]
    return results


def get_history(ticker, start_date=None, end_date=None, error_incomplete_data=False):
    """ dates are YYYY-MM-DD
    """
    if end_date is None:
        end_seconds = int(pd.Timestamp("now").timestamp())
    else:
        end_seconds = int(pd.Timestamp(end_date).timestamp())
    if start_date is None:
        start_seconds = 7223400
    else:
        start_seconds = int(pd.Timestamp(start_date).timestamp())
    site = (
        f"https://finance.yahoo.com/quote/{ticker}/history?period1={start_seconds}"
        f"&period2={end_seconds}&interval=1d&filter=history&frequency=1d"
    )
    period = (end_seconds - start_seconds)/60/60/24  # days
    resp = requests.get(site)
    html = resp.content.decode()
    start = html.index('"HistoricalPriceStore"')
    end = html.index("firstTradeDate")
    needed = html[start:end]
    needed = needed.strip('"HistoricalPriceStore":').strip(""","isPending":false,'""") + "}"
    temp = loads(needed)
    result = json_normalize(temp['prices'])
    result['calendar'] = result['date'].map(lambda x: pd.datetime.fromtimestamp(x).date())
    result['avg'] = result[['open', 'high', 'low', 'close']].mean(axis=1)
    result = result.dropna()
    result = result.reset_index(drop=True)
    result.index = result.calendar.copy()
    del result['calendar']
    result = result.sort_index()
    if error_incomplete_data:
        filled = len(result)/period*100
        # 52*5 (no weekends) - 9 (holidays) = 251 trading days a year, or 69%.
        # Let's throw out tickers that have data on less than half these days.
        if filled <= 35:
            msg = f'percentage of days in the requested time period that have data: {int(filled)}%'
            raise ValueError(msg)
    return result


def get_price(ticker):
    df = get_history(ticker, end_date=(pd.Timestamp.today() + pd.DateOffset(10)))
    return df.close[-1]


def get_stats(ticker):
    site = f"https://finance.yahoo.com/quote/{ticker}/key-statistics?p={ticker}"
    tables = pd.read_html(site)
    table = tables[0]
    for elt in tables[1:]:
        table = table.append(elt)
    table.columns = ["Attribute", "Value"]
    table = table.reset_index(drop=True)
    return table


def get_analysts_info(ticker):
    site = f"https://finance.yahoo.com/quote/{ticker}/analysts?p={ticker}"
    tables = pd.read_html(site, header=0)
    table_names = [table.columns[1] for table in tables]
    results = {key: val for key, val in zip(table_names, tables)}  # pylint: disable=unnecessary-comprehension
    return results


def get_day_gainers(quantity=100):
    site = f"https://finance.yahoo.com/gainers?offset=0&count={quantity}"
    session = HTMLSession()
    resp = session.get(site)
    resp.html.render()
    tables = pd.read_html(resp.html.html)
    df = tables[0].copy()
    df.columns = tables[0].columns
    del df["52 Week Range"]
    df["% Change"] = df["% Change"].map(lambda x: float(x.strip("%")))
    fields_to_change = [x for x in df.columns.tolist() if "Vol" in x or x == "Market Cap"]
    for field in fields_to_change:
        df[field] = unspell_money(df[field])
    session.close()
    return df


def get_day_losers(quantity=100):
    site = f"https://finance.yahoo.com/losers?offset=0&count={quantity}"
    session = HTMLSession()
    resp = session.get(site)
    resp.html.render()
    tables = pd.read_html(resp.html.html)
    df = tables[0].copy()
    df.columns = tables[0].columns
    del df["52 Week Range"]
    df["% Change"] = df["% Change"].map(lambda x: float(x.strip("%")))
    fields_to_change = [x for x in df.columns.tolist() if "Vol" in x or x == "Market Cap"]
    for field in fields_to_change:
        df[field] = unspell_money(df[field])
    session.close()
    return df


def get_slope(ticker,
              start_date=None,
              end_date=None,
              error_incomplete_data=True,
              basis=0):
    """Linear regression for %/year. It is calculated in $/yr, then normalized by
    the stock price. Basis is the index of the stock price used for the
    normalization (0 = price at start date, -1 = price at end date).
    """
    regressor = LinearRegression()
    data = get_history(ticker, start_date, end_date,
                       error_incomplete_data=error_incomplete_data)
    normalization_price = data.avg[basis]
    y = data['avg'].values.reshape(-1, 1)
    x = data['date'].values.reshape(-1, 1)
    regressor.fit(x, y)
    m = regressor.coef_.item()
    dollars_per_year = m*60*60*24*365
    percent_per_year = 100*dollars_per_year/normalization_price
    return percent_per_year


def get_delta(ticker):
    """Returns the change (%) in the past year.
    """
    yld_str = '52-Week Change'
    stats = get_stats(ticker=ticker)
    cell = stats.loc[stats['Attribute'].str.contains(yld_str)]['Value']
    if cell.empty:
        raise ValueError('no 52-week-change cell in the return of get_stats()')
    yld = cell.values[0]
    if isinstance(yld, numbers.Number) and math.isnan(yld):
        raise ValueError('52-week-change is NaN')
    return float(yld.replace('%', ''))


def get_delta_specific(ticker,
                       start_date=None,
                       end_date=None,
                       error_incomplete_data=True,
                       basis=0):
    """Calculates the difference in price over a time period. If the data does not span
    the majority of the requested time, it errors.

    If valid, it will annualize the value in $/yr. If the data spans 6 months, it will
    mulitply the delta by 2. Once it has $/yr, it will normalize by the stock price. Basis
    is the index of the stock price used for the normalization (0 = price at start date,
    -1 = price at end date).
    """
    data = get_history(ticker, start_date, end_date,
                       error_incomplete_data=error_incomplete_data)
    normalization_price = data.avg[basis]
    dollar_delta = data.avg[-1] - data.avg[0]
    time_delta = data.date[-1] - data.date[0]  # seconds
    time_factor = 365*24*60*60/time_delta
    dollars_per_year = dollar_delta*time_factor
    percent_per_year = 100*dollars_per_year/normalization_price
    return percent_per_year


def get_dividend_yield(ticker, mode):
    """Returns the dividend percentage yield (dividend/price).

    Args:
        ticker (str)
        mode (str): Trailing or Forward
    """
    yld_str = f'{mode} Annual Dividend Yield'
    stats = get_stats(ticker=ticker)
    cell = stats.loc[stats['Attribute'].str.contains(yld_str)]['Value']
    if cell.empty:
        raise ValueError('no yield cell in the return of get_stats()')
    yld = cell.values[0]
    if isinstance(yld, numbers.Number) and math.isnan(yld):
        raise ValueError('yield is NaN')
    return float(yld.replace('%', ''))


def plot_history(ticker, start_date=None, end_date=None):
    regressor = LinearRegression()
    data = get_history(ticker, start_date, end_date)
    y = data['avg'].values.reshape(-1, 1)
    x = data['date'].values.reshape(-1, 1)
    regressor.fit(x, y)
    m = regressor.coef_.item()
    b = regressor.intercept_.item()
    pyplot.plot(x, y)
    pyplot.plot(x, m*x+b)
    pyplot.show()
