from setuptools import setup, find_packages
from os import path
from io import open

this_dir = path.abspath(path.dirname(__file__))
with open(path.join(this_dir, 'requirements.txt')) as fh:
    reqs = fh.read().splitlines()


setup(
    name='tradebot',
    version='1.0',
    packages=find_packages(),
    install_requires=reqs,
    entry_points={'console_scripts': [
        'roi-forward = tradebot.analysis:total_return_forward',
        'roi-trailing = tradebot.analysis:total_return_trailing',
    ]},
)
